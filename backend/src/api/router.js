const Router = require('express-promise-router')
const { NotFoundError, ValidationError } = require('objection')
const {
  CheckViolationError,
  ConstraintViolationError,
  DataError,
  DBError,
  ForeignKeyViolationError,
  NotNullViolationError,
  UniqueViolationError
} = require('objection-db-errors')

const user = require('./user')

const router = new Router()

router.get('/', async (_req, res) => {
  res.json({ message: 'Hello World!' })
})

router.get('/users', async (req, res) => {
  res.json({ result: await user.all(req.query) })
})

router.get('*', async (_req, _res) => {
  const err = new Error('404 not found')
  err.status = 404
  throw err
})

router.use(async (err, _req, res, _next) => {
  augmentObjectionError(err)

  const { message, stack, code = null, status = null } = err
  console.error(stack)

  res
    .status(Number(err.status) || 500)
    .json({ error: { message, code, status } })
})

function augmentObjectionError(err, res) {
  if (err instanceof ValidationError) {
    err.status = 400
    err.code = err.type || 'UnknownValidationError'
  } else if (err instanceof NotFoundError) {
    err.status = 404
    err.code = 'NOT_FOUND'
  } else if (err instanceof UniqueViolationError) {
    err.status = 409
    err.code = 'UNIQUE_VIOLATION'
  } else if (err instanceof NotNullViolationError) {
    err.status = 400
    err.code = 'NOT_NULL_VIOLATION'
  } else if (err instanceof ForeignKeyViolationError) {
    err.status = 409
    err.code = 'FOREIGN_KEY_VIOLATION'
  } else if (err instanceof CheckViolationError) {
    err.status = 400
    err.code = 'CHECK_VIOLATION'
  } else if (err instanceof ConstraintViolationError) {
    err.status = 409
    err.code = 'UNKNOWN_CONSTRAINT_VIOLATION'
  } else if (err instanceof DataError) {
    err.status = 400
    err.code = 'UNKNOWN_DATA_ERROR'
  } else if (err instanceof DBError) {
    err.status = 500
    err.code = 'UNKNOWN_DB_ERROR'
  }
}

module.exports = router
