const Knex = require('knex')
const { DbErrors } = require('objection-db-errors')
const { join } = require('path')
const { knexSnakeCaseMappers, Model } = require('objection')

const rootPath = join(__dirname, '../../')
const knexConfig = require('../config/knexfile')

class BaseModel extends DbErrors(Model) {
  static modelPaths = join(rootPath, 'src', 'models')

  static useLimitInFirst = true

  static createNotFoundError(queryContext) {
    const err = this.notFoundError()
    err.message = 'not found'
    err.status = 404
    return err
  }

  $beforeInsert() {
    this.createdAt = new Date()
  }

  $beforeUpdate() {
    this.updatedAt = new Date()
  }
}

const knex = Knex({ ...knexConfig, ...knexSnakeCaseMappers() })

BaseModel.knex(knex)

module.exports = BaseModel
